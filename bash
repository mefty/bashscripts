 #!/bin/bash
sed -e '/<patata>/,$d' repository.xml > repositoryfinal.xml
echo "<patataparizianiki>" >> repositoryfinal.xml
admin="#admin"
package="#package"
readonly="#readonly"
mode=-1

while read p
do
        if [ "$p" == "$admin" ]; then
        mode=0
        elif [ "$p" == "$package" ]; then
        mode=1
        elif [ "$p" == "$readonly" ]; then
        mode=2
        else
                if [ "$mode" -eq "0" ]; then
                        echo "admin $p restofadmin" >> repositoryfinal.xml
                elif [ "$mode" -eq "1" ]; then
                        echo "package $p restofpackage" >> repositoryfinal.xml
                else
                        echo "normal $p restofnormal" >> repositoryfinal.xml
                fi
        fi
done < permissions.txt
echo "</patataparizianiki>" >> repositoryfinal.xml